# warning-ignore:return_value_discarded
extends Control


func _ready() -> void:
	_on_new_game_pressed()


func _on_exit_pressed() -> void:
	get_tree().quit()


func _on_new_game_pressed() -> void:
	get_tree().change_scene("res://main.tscn")


func _on_continue_pressed() -> void:
	pass # we might have continue here one day
