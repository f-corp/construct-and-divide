class_name Spawner, "res://asset/image/class_spawner.svg"
extends Timer
# Spawn enemies based on wave data from json file
# Waves contain data like wait_time and enemy count,
# they also contain enemy properties (health etc)


var waves: Array
var wave: Dictionary
var gui: Label
var is_spawning := false setget set_is_spawning
var current := -1 # Current wave, increments to 0 at first wave
var builder: Builder


func init(wave_file_path: String, builder: Builder, gui: Label) -> void:
	waves = _json(wave_file_path)
	self.gui = gui
	self.builder = builder

	# Additional wave data. range() since we store array index as 'wave_id'
	for i in range(waves.size()):
		waves[i].enemy = load("res://enemy/%s.tscn" % waves[i].enemy)
		waves[i].enemies_remaining = waves[i].count
		waves[i].wave_id = i # Used by class Enemy signals
		waves[i].enemies_spawned = 0

	var err = connect("timeout", self, "_spawn")
	assert(not err, "connect(timeout) failed (%d)" % err)


func start_wave(next: int = current + 1) -> bool:
	if next < waves.size() and not is_spawning:
		current = next
		wave = waves[current]
		wait_time = wave.wait_time
		gui.text = "%d/%d" % [(current + 1), waves.size()]
		self.is_spawning = true
	return is_spawning


func _input(event: InputEvent) -> void:
	if event.is_action_pressed("ui_select"):
		start_wave()


func _spawn() -> void:
	var e = wave.enemy.instance()
	e.init(wave, owner, self)
	e.spawn()

	wave.enemies_spawned += 1
	if wave.enemies_spawned == wave.count:
		self.is_spawning = false


func set_is_spawning(state: bool) -> void:
	is_spawning = state
	if is_spawning:
		start()
	else:
		stop()


# Signaled by enemies when removed (killed/despawned)
func _decrease_enemies_remaining(id: int) -> void:
	waves[id].enemies_remaining -= 1
	if waves[id].enemies_remaining == 0:
		builder.resources += waves[id].bonus


func _json(path: String):
	var json: JSONParseResult
	var file = File.new()

	assert(file.file_exists(path), "%s doesn't exist" % path)
	file.open(path, File.READ)
	json = JSON.parse(file.get_as_text())
	file.close()
	assert(not json.error, "JSON parse of %s failed (%d)" % [path, json.error])
	return json.result
