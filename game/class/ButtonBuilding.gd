class_name ButtonBuilding, "res://asset/image/class_buttonbuilding.svg"
extends Button
# Scene to build when clicked can be set in the inspector


signal build_button_pressed(building)

export(PackedScene) var build_this_scene
export(int, "none", "1", "2", "3", "4", "5", "6", "7", "8", "9") var shortcut_number
var cost := INF

func _ready() -> void:
	var error = connect("build_button_pressed", owner.get_node("builder"), "toggle")
	assert(not error, "connect(build_button_pressed) failed (%d)" % error)
	if build_this_scene is PackedScene:
		var building = build_this_scene.instance()
		cost = building.cost
		building.queue_free()
		if shortcut_number:
			shortcut = ShortCut.new()
			shortcut.shortcut = InputEventAction.new()
			shortcut.shortcut.action = "kb_%s" % shortcut_number
	else:
		disabled = true


func _pressed() -> void:
	emit_signal("build_button_pressed", build_this_scene)


func check_can_afford(resources: int) -> void:
	if cost <= resources:
		disabled = false
	else:
		disabled = true
