class_name StrategyCamera, "res://asset/image/class_strategycamera.svg"
extends Spatial
# Camera with movement locked at a spacial
# - WASD movement
# - QE rotation
# - Scroll "zoom"


export var camera_origin := Vector3(0,30,10)
export var move_speed := 20
export var scroll_speed := 25
export var rotate_speed := 0.9


var camera := Camera.new()


func _ready():
	add_child(camera)
	camera.current = true
	camera.translate(camera_origin)
	camera.fov = 70


func _process(delta: float) -> void:
	camera.look_at(self.translation, (Vector3.UP))
	var move = move_speed * delta
	var scroll = scroll_speed * delta
	var rotate = rotate_speed * delta

	if Input.is_action_pressed("w"):
		translate(Vector3(0, 0, -move))
	if Input.is_action_pressed("a"):
		translate(Vector3(-move, 0, 0))
	if Input.is_action_pressed("s"):
		translate(Vector3(0, 0, move))
	if Input.is_action_pressed("d"):
		translate(Vector3(move, 0, 0))

	if Input.is_action_pressed("q"):
		rotate_y(-rotate)
	if Input.is_action_pressed("e"):
		rotate_y(rotate)

	if Input.is_action_just_released("scroll_in"):
		camera.translate(Vector3(0, -scroll, -scroll))
	if Input.is_action_just_released("scroll_out"):
		camera.translate(Vector3(0, scroll, scroll))
