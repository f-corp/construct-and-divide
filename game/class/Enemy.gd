class_name Enemy, "res://asset/image/class_enemy.svg"
extends PathFollow
# Handles enemies. Damage received, spawn, despawn etc.
# Enemies cannot be instanced without a map in the parent scene


signal enemy_despawned
signal enemy_killed
signal enemy_removed(wave_id)

const ENEMY_SPEED_MODIFIER := 0.1

var health: int
var max_health: int
var speed: float
var size := 1.0
var animation_player: AnimationPlayer
var animation: Animation
var resources: int
var path: Path
var curve_length: float
var wave_index: int


func init(properties: Dictionary, main: Spatial, spawner: Spawner) -> void:
	for key in properties.keys():
		set(key, properties[key])
	max_health = health
	scale = Vector3(size, size, size)

	animation_player = get_node("AnimationPlayer")
	animation = animation_player.get_animation("move")
	animation.loop = true

	path = main.map.get_node("Path")
	curve_length = path.curve.get_baked_length()
	rotation_mode = ROTATION_Y
	loop = false

	var error
	error = connect("enemy_despawned", main, "_on_enemy_despawned")
	assert(not error, "connect(enemy_despawned) failed (%d)" % error)
	error = connect("enemy_killed", main.builder, "add_resources")
	assert(not error, "connect(enemy_killed) failed (%d)" % error)
	error = connect("enemy_removed", spawner, "_decrease_enemies_remaining")
	assert(not error, "connect(enemy_removed) failed (%d)" % error)

func _process(delta: float) -> void:
	offset += delta * speed * ENEMY_SPEED_MODIFIER
	if offset >= curve_length:
		despawn()


func spawn() -> void:
	path.add_child(self)
	animation_player.play("move")


func despawn() -> void:
	emit_signal("enemy_despawned")
	remove()


func die() -> void:
	emit_signal("enemy_killed", resources)
	remove()


func remove() -> void:
	emit_signal("enemy_removed", wave_index)
	self.queue_free()


func take_damage(damage: int) -> void:
	health -= damage
	if health <= 0:
		die()
